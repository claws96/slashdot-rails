Rails.application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  root 'application#index'

  get  '/' => 'application#index'
  post '/' => 'application#new_search'

end
