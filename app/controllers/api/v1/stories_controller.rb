class Api::V1::StoriesController < ActionController::Base
  protect_from_forgery with: :null_session
  rescue_from ActionController::ParameterMissing, with: :handle_error_response

  def index
    display_results
    render json: {"total_entries": @stories_list.total_entries,
                  "current_page": @stories_list.current_page,
                  "total_pages": total_pages_count,
                  "entry_list": @stories_list}.to_json
  end

  def display_results
    results = Stories.get_stories permitted_params[:sortby], permitted_params
    @stories_list = results.paginate page: permitted_params[:page], per_page: ApplicationController::PER_PAGE
  end

  def total_pages_count
    total_pages = @stories_list.total_entries / ApplicationController::PER_PAGE
    ApplicationController::PER_PAGE * total_pages == @stories_list.total_entries ? total_pages : total_pages + 1
  end

  def permitted_params
    params.require(:filter).permit( :title, :intro, :start_date, :end_date, :reset, :sortby, :page ).tap do |param|
      param.merge!({sortby: ApplicationController::DEFAULT_SORT}) if param[:sortby].nil?
      param.merge!({page: 1}) if param[:page].nil?
    end
  end

  def handle_error_response
    render json: {error: "DATA MISSING"}.to_json, status: 400
  end

end
