class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  PER_PAGE     = 25.freeze
  DEFAULT_SORT = "date DESC".freeze

  def index
 
    paginate if params[:page]

    reset_session_params unless params[:page]

  end

  def new_search

    reset_session_params if reset_pressed?
	  
    set_session_params unless reset_pressed?

    render :index

  end

  def paginate

    reset_session_params unless session[:filter] and session[:sortby]

    session[:page] = params[:page]

  end

  def display_results

    results = Stories.get_stories session[:sortby], session[:filter]

    @stories_list = results.paginate page: session[:page], per_page: PER_PAGE

  end

  def render *args

    display_results
    @filter = session[:filter]
    super :index

  end

  def set_session_params
    session[:filter].update permitted_params
    session[:page] = 1
  end

  def reset_session_params
      session[:filter] =  { 'start_date' => '',
                            'end_date'   => '',
                            'intro'      => '',
                            'title'      => ''  }
      session[:sortby] = DEFAULT_SORT
  end

  def reset_pressed?
    params.key?(:reset)
  end

  def permitted_params
    params.require(:filter).permit( :title, :intro, :start_date, :end_date, :reset )  
  end

end
