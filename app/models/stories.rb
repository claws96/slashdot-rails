class Stories < ActiveRecord::Base

  validates_presence_of :id_hash, :title, :author,  :date, :dept, :intro, :link, message: '300'
  validates_format_of :link, with: /\Ahttp:\/\/.*\.slashdot\.org\/.*/i, message: '200'
  validates_uniqueness_of :id_hash, message: '100'

  before_validation do
    self.id_hash = SlashStory::generate_id_hash self.attributes
  end

  def self.get_stories(sortby, filters={})

    start_date = filters['start_date'] == '' ? negative_infinity : Date.strptime(filters['start_date'], '%m/%d/%Y')
    end_date   = filters['end_date']   == '' ? positive_infinity : Date.strptime(filters['end_date'], '%m/%d/%Y')

    self
        .select('id,date,title,link,intro')
        .where("title LIKE ?", "%#{filters['title']}%")
        .where("intro LIKE ?", "%#{filters['intro']}%")
        .where("date between ? and ?", start_date, end_date)
        .order(sortby)

  end

  def self.positive_infinity
    # is there a method that would tell me?
    case ActiveRecord::Base.connection_config[:adapter]
      when 'postgresql'
        return 'infinity'
      when 'mysql'
        return '9999-99-99'
    end


  end

  def self.negative_infinity
    case ActiveRecord::Base.connection_config[:adapter]
      when 'postgresql'
        return '-infinity'
      when 'mysql'
        return '0000-00-00'
    end
  end

end
