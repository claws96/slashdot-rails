require 'active_support'
require 'test_helper'

class ApplicationControllerTest < ActionDispatch::IntegrationTest

    test 'should load catalog index page' do
      ids_from_html = []

      get "/"
      assert_equal 200, @response.status
      assert_select 'title', 'Slashdot Archive'

      body_tags = Nokogiri::HTML @response.body

      body_tags.css('table#results tr').each do |el|
        story_id = el['data-story-id']
        ids_from_html.push story_id if story_id
      end

      ids_from_db = Stories.order('date DESC').pluck('id')

      assert_equal 25, ids_from_html.count

      assert_nil ids_from_db <=> ids_from_html

    end

    test 'should load page 2 of catalog index page' do
      id_numbers = []

      get "/", params: { page: 2 }
      assert_equal 200, @response.status
      assert_select 'title', 'Slashdot Archive'

      body_tags = Nokogiri::HTML @response.body

      body_tags.css('table#results tr').each do |el|
        story_id = el['data-story-id']
        id_numbers.push story_id if story_id
      end

      assert_equal 2, id_numbers.count

    end

end
