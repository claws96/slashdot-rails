require 'active_support'
require 'test_helper'

class Api::V1::StoriesControllerTest < ActionDispatch::IntegrationTest
  test "should return whole stories list" do
    post "/v1/stories",
         params: {
             'filter': {
                 'title': '',
                 'intro': '',
                 'start_date': '',
                 'end_date': ''
             }
         }
    assert_equal 200, @response.status

    parsed  = JSON.parse @response.body
    stories = JSON.parse Stories.select('id,date,link,title,intro').limit(ApplicationController::PER_PAGE).to_json

    assert_equal 1, parsed['current_page']
    assert_equal 2, parsed['total_pages']
    assert_equal stories.count, ApplicationController::PER_PAGE
    assert_equal stories, parsed['entry_list']
  end

  test "should return page 2 of whole stories list" do
    # There are 27 stories in the fixtures.
    post "/v1/stories",
         params: {
             'filter': {
                 'title': '',
                 'intro': '',
                 'start_date': '',
                 'end_date': '',
                 'page': 2
             }
         }
    assert_equal 200, @response.status

    parsed  = JSON.parse @response.body
    stories = JSON.parse Stories.select('id,date,link,title,intro')
                             .limit(ApplicationController::PER_PAGE)
                             .offset(ApplicationController::PER_PAGE).to_json

    assert_equal 2, parsed['current_page']
    assert_equal 2, parsed['total_pages']
    assert_equal stories.count, parsed['entry_list'].count
    assert_equal stories, parsed['entry_list']
  end

  test "should search for title" do
    search_title = "Security"

    post "/v1/stories",
         params: {
             'filter': {
                 'title': search_title,
                 'intro': '',
                 'start_date': '',
                 'end_date': ''
             }
         }
    assert_equal 200, @response.status

    parsed  = JSON.parse @response.body
    stories = JSON.parse Stories.select('id,date,link,title,intro')
                             .where("title LIKE ?", "%#{search_title}%")
                             .limit(ApplicationController::PER_PAGE).to_json

    assert_equal 1, parsed['current_page']
    assert_equal 1, parsed['total_pages']
    assert_equal stories.count, parsed['entry_list'].count
    assert_equal stories, parsed['entry_list']
  end

  test "should search for word in intro" do
    search_intro = "Amazon has filed a patent"

    post "/v1/stories",
         params: {
             'filter': {
                 'title': '',
                 'intro': search_intro,
                 'start_date': '',
                 'end_date': ''
             }
         }
    assert_equal 200, @response.status

    parsed  = JSON.parse @response.body
    stories = JSON.parse Stories.select('id,date,link,title,intro')
                                .where("intro LIKE ?", "%#{search_intro}%")
                                .limit(ApplicationController::PER_PAGE).to_json

    assert_equal 1, parsed['current_page']
    assert_equal 1, parsed['total_pages']
    assert_equal stories.count, parsed['entry_list'].count
    assert_equal stories, parsed['entry_list']
  end

  test "should find no stories before a certain date" do
    # All stories in fixtures are 03-15-2016 and should be in mm/dd/yyyy
    search_date = "03/14/2016"

    post "/v1/stories",
         params: {
             'filter': {
                 'title': '',
                 'intro': '',
                 'start_date': '',
                 'end_date': search_date
             }
         }
    assert_equal 200, @response.status

    parsed  = JSON.parse @response.body
    stories = JSON.parse Stories.select('id,date,link,title,intro')
                                .where("date between ? and ?", Stories.negative_infinity, "%#{search_date}")
                                .limit(ApplicationController::PER_PAGE).to_json

    assert_equal 1, parsed['current_page']
    assert_equal 0, parsed['total_pages']
    assert_equal stories.count, parsed['entry_list'].count
    assert_equal stories, parsed['entry_list']
  end

  test "should find no stories after a certain date" do
    # All stories in fixtures are 03-15-2016 and should be in mm/dd/yyyy
    search_date = "03/16/2016"

    post "/v1/stories",
         params: {
             'filter': {
                 'title': '',
                 'intro': '',
                 'start_date': search_date,
                 'end_date': ''
             }
         }
    assert_equal 200, @response.status

    parsed  = JSON.parse @response.body
    stories = JSON.parse Stories.select('id,date,link,title,intro')
                                .where("date between ? and ?", "%#{search_date}", Stories.positive_infinity)
                                .limit(ApplicationController::PER_PAGE).to_json

    assert_equal 1, parsed['current_page']
    assert_equal 0, parsed['total_pages']
    assert_equal stories.count, parsed['entry_list'].count
    assert_equal stories, parsed['entry_list']
  end

  test "should find all stories on a certain bounded date" do
    # All stories in fixtures are 03-15-2016 and should be in mm/dd/yyyy
    search_date = "03/15/2016"

    post "/v1/stories",
         params: {
             'filter': {
                 'title': '',
                 'intro': '',
                 'start_date': search_date,
                 'end_date': search_date
             }
         }
    assert_equal 200, @response.status

    parsed  = JSON.parse @response.body
    stories = JSON.parse Stories.select('id,date,link,title,intro')
                             .where("date between ? and ?", "%#{search_date}", "%#{search_date}")
                             .limit(ApplicationController::PER_PAGE).to_json

    assert_equal 1, parsed['current_page']
    assert_equal 2, parsed['total_pages']
    assert_equal stories.count, parsed['entry_list'].count
    assert_equal stories, parsed['entry_list']
  end

  test 'should handle erroneous dates' do
    skip 'should handle erroneous dates'
  end

  test 'get request should create errors' do
    # We want to be purposefully vague with errors to avoid leaking implementation details.
    get '/v1/stories'

    assert_equal 400, @response.status
    assert_equal "DATA MISSING", JSON.parse(@response.body)['error']

  end

  test 'empty post should create errors' do
    post "/v1/stories",
         params: {}

    assert_equal 400, @response.status
    assert_equal "DATA MISSING", JSON.parse(@response.body)['error']

  end
end
