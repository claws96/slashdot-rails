require 'active_support'
require 'test_helper'
require 'slashstory_fixtures'

class SlashStoryLibTest < ActiveSupport::TestCase

   test 'should retrieve yesterdays slashdot index page' do

     assert ENV['LOGIN_COOKIE'], "Login cookie is not set....tests will fail!!"

     content = Nokogiri.HTML SlashStory::get_page(Date.new(2016,3,15))

     title = content.css('title').text.downcase.index('news for nerds')
     # ensure that we're logged in:
     refute_empty content.css('a[href="//slashdot.org/~claws"]').text, 'Be sure to set LOGIN_COOKIE'

     assert_not_nil title

   end

  test 'should parse a slashdot index page' do

    # Tests against the data automatically loaded into the db from the fixture

    fixtures_hashes = []
    parsed_stories = SlashStory::parse_stories SlashStoryFixtures::sample_index_file

    fixtures_hashes = parsed_stories.values.map { |s| SlashStoryFixtures::generate_id_hash s[:title],
                                                                                           s[:author],
                                                                                           s[:dept],
                                                                                           s[:intro] }

    assert_equal fixtures_hashes.count, Stories.where(id_hash: fixtures_hashes).count

  end

  test 'should store parsed stories in sql db' do

    fixtures_hashes = []
    parsed_stories_fixtures = YAML.load_file("#{Rails.root}/test/fixtures/stories.yml")
    Stories.delete_all

    assert_equal 0, Stories.count

    parsed_stories_fixtures.each do |index, s|
      fixtures_hashes.push s[:id_hash].to_s
      s[:id_hash] = ''
    end

    SlashStory::do_sql parsed_stories_fixtures

    assert_equal fixtures_hashes.count, Stories.where(id_hash: fixtures_hashes).count

  end

  test 'should not store an existing story' do
    duplicate_story = {}
    count_before_test = Stories.count
    parsed_stories_fixtures = YAML.load_file("#{Rails.root}/test/fixtures/stories.yml")

    duplicate_story['82089611'] = parsed_stories_fixtures['82089611']
    duplicate_story['82089611'][:id_hash] = ''

    SlashStory::do_sql duplicate_story

    assert_equal count_before_test, Stories.count

  end

  test 'should return last date stored in database' do
    assert_equal SlashStory::last_recorded_date, Stories.order(:date).first.date
  end

  test 'should return todays date because of empty db' do
    Stories.delete_all

    assert_equal SlashStory::last_recorded_date, Date.yesterday
  end

end
