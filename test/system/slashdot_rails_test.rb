require "application_system_test_case"

class SlashdotRailsTest < ApplicationSystemTestCase
  test "visiting the index" do
    visit '/'

    assert_equal "Slashdot Archive", page.title
  end

  test "listing the titles" do
    visit '/'

    stories_count = Stories.all.order(nil).count
    
    assert page.has_content? stories_count.to_s + ' Results'

    Stories.all.order(ApplicationController::DEFAULT_SORT).limit(ApplicationController::PER_PAGE).each do |s|
      assert page.has_content? s.title
    end
  end

  test "searching on title" do
    visit '/'

    fill_in 'filter_title', with: 'Comcast'
    click_button 'Search'

    assert page.has_content? '1 Results'
    
    within 'td.intro' do
      assert page.has_css? 'span.results-hilight', text: 'Comcast'
    end

    within 'td.link a' do
      assert page.has_css? 'span.results-hilight', text: 'Comcast'
    end

  end

  test "searching on intro" do
    visit '/'

    fill_in 'filter_intro', with: 'internet'
    click_button 'Search'

    assert page.has_content? '1 Results'
    
    within 'td.link a' do
      assert page.has_no_css? 'span.results-hilight', text: 'internet'
    end
    
    within 'td.intro' do
      assert page.has_css? 'span.results-hilight', text: 'internet'
    end
  
  end

  test "searching on start date" do
    # datepicker is broken
    visit '/'

    fill_in 'filter_start_date', with: '3/16/2017'
    click_button 'Search'

    assert page.has_content? '0 Results'
  end
  
  test "searching on end date" do
    # datepicker is broken
    visit '/'

    fill_in 'filter_end_date', with: '3/14/2016'
    click_button 'Search'

    assert page.has_content? '0 Results'
  end

  test "reset button" do
    # datepicker is broken
    visit '/'

    fill_in 'filter_title', with: 'Test Title'
    fill_in 'filter_intro', with: 'Test Intro'
    fill_in 'filter_start_date', with: '3/16/2017'
    fill_in 'filter_end_date', with: '3/14/2016'

    click_button 'Reset'

    assert page.has_selector? 'input[id="filter_title"][value=""]'
    assert page.has_selector? 'input[id="filter_intro"][value=""]'
    assert page.has_selector? 'input[id="filter_start_date"][value=""]'
    assert page.has_selector? 'input[id="filter_end_date"][value=""]'
  end
      
end
