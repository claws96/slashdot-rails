#!/bin/env ruby
# encoding: utf-8

require 'net/http'

class SlashStory
  def self.get_page(get_date)
    uri = URI.parse "https://slashdot.org/?issue=#{get_date.strftime('%Y%m%d')}"

    Rails.logger.info "RequestURL is #{uri}"

    req           = Net::HTTP::Get.new uri
    req['Cookie'] = CGI::Cookie.new('name'   => 'user', 
                                    'path'   => '/',
                                    'domain' => 'slashdot.org',
                                    'value'  => ENV['LOGIN_COOKIE']).to_s
    req['User-Agent'] = "Lynx/2.8.7rel.1 libwww-FM/2.14 SSL-MM/1.4.1 OpenSSL/0.9.8q"
    res               = Net::HTTP.new uri.host, uri.port
    res.use_ssl       = true
    response          = res.request req

    options = { :invalid => :replace,
                  :undef => :replace,
                :replace => '?' }

    response.body.encode('UTF-8', **options)
  end

  def self.parse_stories(body)
    stories = Hash.new
    page    = Nokogiri::HTML(body)

    page.css('span[id^=title-]').each do |l|
      link_info = l.css('a')
      link_id   = l[:id].match(/^title-(.*)/)[1]

      stories[link_id] = {    id: link_id,
                            link: "http:#{link_info[0]['href']}",
                           title: link_info.text.strip } 
    end

    page.css('div[id^=details-]').each do |l|
      link_id = l[:id].match(/^details-(.*)/)[1]
      
      stories[link_id].merge!({ author: l.text.strip.match(/(Submitted|Posted)\s+by\s+(.*)/)[2],
                                  date: l.css('time').text.match(/on (.*)/u)[1],
                                  dept: l.css('span.dept-text').text.strip })   
    end

    page.css('div[id^=text-]').each do |l|
      link_id = l[:id].match(/^text-(.*)/)[1]

      stories[link_id].merge!({ intro: l.text.strip })
    end

    Rails.logger.info "#{stories.count} stories parsed."

    stories
  end

  def self.do_sql(stories)
    stories.each do |index, s|

      t =  Stories.create s.except(:id)

      indicator = t.valid? ? 'O' : 'X'

      msg = "#{s[:title].to_s}   #{s[:link]} #{indicator}\n"  

      puts msg
      Rails.logger.info msg

    end
  end

  def self.last_recorded_date
      t = Stories.select(:date).order("date DESC").first

      t.nil? ? Date.yesterday : t.date

  end

  def self.generate_id_hash(attrs)
    Digest::SHA1.hexdigest '' + attrs['title'] + attrs['author'] + attrs['dept'] + attrs['intro']
  end
end
