CREATE OR REPLACE FUNCTION public.update_stories_tsv()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
BEGIN
  UPDATE stories SET title_ts = to_tsvector('english', NEW.title) WHERE id = NEW.id;
  UPDATE stories SET author_ts = to_tsvector('english', NEW.author) WHERE id = NEW.id;
  UPDATE stories SET dept_ts = to_tsvector('english', NEW.dept) WHERE id = NEW.id;
  UPDATE stories SET intro_ts = to_tsvector('english', NEW.intro) WHERE id = NEW.id;
  RETURN NEW;
END $function$
CREATE trigger update_stories_tsvectors AFTER INSERT ON stories FOR EACH ROW EXECUTE PROCEDURE update_stories_tsv();
ALTER FUNCTION update_stories_tsv()
  OWNER TO claws;
