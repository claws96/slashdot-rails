# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 0) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "stories", id: :bigserial, force: :cascade do |t|
    t.date     "date"
    t.text     "link"
    t.text     "title"
    t.tsvector "title_ts"
    t.text     "intro"
    t.tsvector "intro_ts"
    t.text     "dept"
    t.tsvector "dept_ts"
    t.text     "author"
    t.tsvector "author_ts"
    t.string   "id_hash",   limit: 40
  end

  add_index "stories", ["author_ts"], name: "author_ts_idx", using: :gin
  add_index "stories", ["dept_ts"], name: "dept_ts_idx", using: :gin
  add_index "stories", ["id_hash"], name: "id_hash_idx", unique: true, using: :btree
  add_index "stories", ["intro_ts"], name: "intro_ts_idx", using: :gin
  add_index "stories", ["title_ts"], name: "title_ts_idx", using: :gin

end
